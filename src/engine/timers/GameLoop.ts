import { EventSet } from '../events/EventSet';

const startEvent: string = 'start';
const stopEvent: string = 'stop';
const tickEvent: string = 'update';

export class GameLoop extends EventSet {

    private clock: any;
    private lastTime: number;
    private lastDelta: number;
    private fps: number;

    constructor() {
        super();
        this.fps = 1000 / 60;
    }

    public start() {
        if (!this.clock) {
            let now = Date.now();
            this.lastTime = now;
            this.clock = setInterval(() => {
                this.loop();
            }, this.fps);

            this.emit(startEvent, now);
        }

        return this;
    }

    private loop() {
        let now = Date.now();
        let delta = (now - this.lastTime) / 1000;
        this.emit(tickEvent, delta, this.lastTime);
    }

    public stop() {
        if (this.clock) {
            clearInterval(this.clock);
            this.emit(stopEvent, Date.now());
        }
    }
}
