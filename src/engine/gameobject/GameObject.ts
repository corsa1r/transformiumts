import { ComponentsContainer } from '../storage/ComponentsContainer';

export class GameObject {

    public components: ComponentsContainer;

    constructor() {
        this.components = new ComponentsContainer();
        this.components.setTarget(this);
    }
}