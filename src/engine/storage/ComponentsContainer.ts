import { Container } from './Container';
import { GameObject } from '../gameobject/GameObject';

export class ComponentsContainer extends Container {

    private targetReference: GameObject;

    constructor() {
        super();
    }

    public setTarget(targetReference: GameObject) {
        this.targetReference = targetReference;
    }
}